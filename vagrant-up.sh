#!/bin/bash

set -e

echo '######################'
echo 'BEGIN: time vagrant up'
echo '######################'
time vagrant up
echo ' /|\'
echo '/ | \'
echo '  |  '
echo 'Time used to provision the Vagrant box'
echo '======================================'
echo '#########################'
echo 'FINISHED: time vagrant up'
echo '#########################'
