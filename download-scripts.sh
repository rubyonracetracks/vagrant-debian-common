#!/bin/bash

set -e

source variables.sh

echo '----------------------------------'
echo "git clone $DOCKER_URL docker-clone"
git clone $DOCKER_URL docker-clone
wait
