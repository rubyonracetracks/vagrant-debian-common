#!/bin/bash

# This script destroys all Vagrant boxes.
# SOURCE:
# https://www.jeffgeerling.com/blog/2016/remove-all-your-local-vagrant-boxes-bash-command

vagrant box list | cut -f 1 -d ' ' | xargs -L 1 vagrant box remove -f
