#!/bin/bash

set -e

wget -O - https://gitlab.com/rubyonracetracks/vagrant-debian-common/raw/master/check-login.sh | bash

git pull origin master

time wget -O - https://gitlab.com/rubyonracetracks/vagrant-debian-common/raw/master/download-scripts.sh | bash

time bash exec-build.sh
echo '======================================='
echo ' /|\'
echo '/ | \'
echo '  |  '
echo 'Time used to build the Vagrant box file'
echo '======================================='

time bash upload.sh
echo ' /|\'
echo '/ | \'
echo '  |  '
echo '========================================'
echo 'Time used to upload the Vagrant box file'
echo '========================================'
