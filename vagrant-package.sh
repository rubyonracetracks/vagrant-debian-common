#!/bin/bash

set -e

source variables.sh

echo '##############################################'
echo "BEGIN: time vagrant package --output $BOX_FILE"
echo '##############################################'
time vagrant package --output $BOX_FILE
echo ' /|\'
echo '/ | \'
echo '  |  '
echo 'Time used to build the Vagrant box file'
echo '======================================='
echo '#################################################'
echo "FINISHED: time vagrant package --output $BOX_FILE"
echo '#################################################'
