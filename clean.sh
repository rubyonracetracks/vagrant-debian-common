#!/bin/bash

echo '------------------'
echo 'BEGIN: cleaning up'
echo '------------------'

rm *.log
rm -rf docker-*
rm *.box
rm Vagrantfile
wait

for vvm in $(VBoxManage list vms | grep "$DISTRO-$SUITE-$ABBREV")
do
  # Virtual machine to remove: ${vvm}
  vm_id=$(echo "${vvm}" | awk '{print substr($2, 2, length($2) - 2)}')
  vm_name=$(echo "${vvm}" | awk '{print substr($1, 2, length($1) - 2)}')

  # Powering off: ${vm_id}
  VBoxManage controlvm ${vm_name} poweroff

  # Unregistering: ${vm_id}
  VBoxManage unregistervm ${vm_name}
done

echo '---------------------'
echo 'FINISHED: cleaning up'
echo '---------------------'
