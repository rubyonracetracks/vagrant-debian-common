#!/bin/bash

set -e

source variables.sh

# Configure the Vagrantfile
echo '---------------------------'
echo 'Configuring the Vagrantfile'
wget https://gitlab.com/rubyonracetracks/vagrant-debian-common/raw/master/Vagrantfile-build
mv Vagrantfile-build Vagrantfile
sed -i.bak "s|<VAGRANT_SOURCE>|$VAGRANT_SOURCE|g" Vagrantfile
sed -i.bak "s|<ABBREV>|$ABBREV|g" Vagrantfile
rm Vagrantfile.bak

echo '#################'
echo 'BEGIN: vagrant up'
echo '#################'
vagrant up
echo '####################'
echo 'FINISHED: vagrant up'
echo '####################'

echo '#########################################'
echo "BEGIN: vagrant package --output $BOX_FILE"
echo '#########################################'
vagrant package --output $BOX_FILE
echo '############################################'
echo "FINISHED: vagrant package --output $BOX_FILE"
echo '############################################'
