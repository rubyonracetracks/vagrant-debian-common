#!/bin/bash

set -e

source variables.sh

# Upload a small file to SourceForge
DATE=`date +%Y-%m%d-%H%M%S`
mkdir -p log
echo "Testing upload - $BOX_NAME - $DATE" > test-upload.txt
echo '####################################################################################################'
echo "rsync -avPz -e ssh test-upload.txt jhsu802701@frs.sourceforge.net:/home/frs/p/vagrant-$DISTRO-$SUITE"
rsync -avPz -e ssh test-upload.txt jhsu802701@frs.sourceforge.net:/home/frs/p/vagrant-$DISTRO-$SUITE
