#!/bin/bash

set -e

source variables.sh

if [[ -z "$GITLAB_CI" && -z "$TRAVIS" ]]
then
  wget -O - https://gitlab.com/rubyonracetracks/vagrant-debian-common/raw/master/clean.sh | bash
fi

wget -O - https://gitlab.com/rubyonracetracks/vagrant-debian-common/raw/master/check-login.sh | bash

wget -O - https://gitlab.com/rubyonracetracks/vagrant-debian-common/raw/master/download-scripts.sh | bash

wget -O - https://gitlab.com/rubyonracetracks/vagrant-debian-common/raw/master/update-vagrantfile.sh | bash

wget -O - https://gitlab.com/rubyonracetracks/vagrant-debian-common/raw/master/vagrant-up.sh | bash

time wget -O - https://gitlab.com/rubyonracetracks/vagrant-debian-common/raw/master/vagrant-package.sh | bash
