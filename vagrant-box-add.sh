#!/bin/bash

echo '--------------------------------------------'
echo "vagrant box remove $VAGRANT_BOX_NAME --force"
vagrant box remove $VAGRANT_BOX_NAME --force

echo '-----------------------------------------'
echo "vagrant box add $VAGRANT_BOX_NAME $LOCAL_BOX_FILE"
vagrant box add $VAGRANT_BOX_NAME $LOCAL_BOX_FILE;
