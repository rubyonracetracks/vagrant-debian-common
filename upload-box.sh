#!/bin/bash

set -e

source variables.sh

echo '###################################################################################################'
echo "time rsync -avPz -e ssh $BOX_FILE jhsu802701@frs.sourceforge.net:/home/frs/p/vagrant-$DISTRO-$SUITE"
time rsync -avPz -e ssh $BOX_FILE jhsu802701@frs.sourceforge.net:/home/frs/p/vagrant-$DISTRO-$SUITE
echo ' /|\'
echo '/ | \'
echo '  |  '
echo '========================================'
echo 'Time used to upload the Vagrant box file'
echo '========================================'
